import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import 'ag-grid-enterprise';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AfPlanningModule } from './af-planning/af-planning.module';
import { AfAgGridModule } from './af-ag-grid/af-ag-grid.module';
import { Ng2CompleterModule } from 'ng2-completer';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AfPlanningModule,
    AfAgGridModule,
    Ng2CompleterModule
  ],
  providers: [],
  declarations: [AppComponent],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
