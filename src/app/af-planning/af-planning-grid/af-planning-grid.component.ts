import { Component } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridBasicFactory } from 'src/app/af-ag-grid/af-ag-grid-basic.factory';
import { AgGridAggregatorFactory } from 'src/app/af-ag-grid/af-ag-grid-aggregator.factory';
import { RowData } from '../../hardcoded-data/row-data';
import { ColumnData } from '../../hardcoded-data/column-data';
import { DropdownComponent } from 'src/app/af-ag-grid/af-ag-components/dropdown/dropdown.component';

@Component({
  selector: 'app-af-planning-grid',
  templateUrl: './af-planning-grid.component.html',
  styleUrls: ['./af-planning-grid.component.scss']
})
export class AfPlanningGridComponent {
  public rangeSum;
  public rangeMin;
  public rangeMax;
  public rangeAvg;
  public gridOptions: GridOptions;

  constructor() {
    const gridOptions = {
      frameworkComponents: {
        editComp: DropdownComponent
      },
      defaultColDef: {
        // width: 175,
        // editable: true
      },
      columnTypes: {}
    };

    AgGridBasicFactory(gridOptions, {});
    AgGridAggregatorFactory(gridOptions, {
      aggStatusPanel: [],
      agg: {}
    });
    this.gridOptions = gridOptions;
    this.gridOptions.rowData = RowData;
    this.gridOptions.columnDefs = ColumnData;
  }

  ngOnInit() {}
}
