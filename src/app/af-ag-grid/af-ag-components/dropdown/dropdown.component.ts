import { Component, Input, ElementRef, ViewChild } from '@angular/core';
import { AgEditorComponent } from 'ag-grid-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { ICellEditorParams } from 'ag-grid-community';
import { ColumnData } from '../../../hardcoded-data/column-data';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements AgEditorComponent {
  @Input() name: String;
  public dropdownData = ColumnData[0].cellEditorParams.values;
  public myForm: FormGroup;
  public oldValue: String;
  public selected: Boolean;
  public params: ICellEditorParams;

  @ViewChild('agInput', { static: true }) public element: ElementRef;

  constructor(private builder: FormBuilder, private _sanitizer: DomSanitizer) {
    this.myForm = this.builder.group({
      gridDropdown: ''
    });
  }

  set value(val) {
    this.myForm.get('gridDropdown').setValue(val);
  }
  get value() {
    return this.myForm.get('gridDropdown').value;
  }

  isPopup(): boolean {
    return false;
  }

  getValue(): String {
    if (this.value === '') {
      this.value = this.oldValue;
    }
    return this.value;
  }

  agInit(params: ICellEditorParams) {
    this.value = params.value;
    this.oldValue = this.value;
    this.value = '';
    return this.value;
  }

  autocompleListFormatter = (data: any) => {
    let html = `<span>${data.name}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  };

  setFocus() {
    this.element.nativeElement.focus();
  }

  ngAfterViewInit() {
    Promise.resolve().then(() => this.setFocus());
  }
}
